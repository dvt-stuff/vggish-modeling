
# coding: utf-8

from readtfrecords import readTfRecordSamples as readtf
import tensorflow as tf
import sys

class printkw():
    import sys
    def __lshift__(self, value=None):
        print(value, file=sys.stderr)
        return value

# filename = "2q.tfrecord"

def get_numbers_list(tfrecords_filename):
    import numpy as np

    obj = readtf(tfrecords_filename=tfrecords_filename)    
    obj = list(obj)[0].ListFields()[1][1]    
    obj = list(obj.ListFields()[0][1].items())[0]
    obj = obj[1].ListFields()[0][1]   
    obj = [x.ListFields()[0][1].ListFields()[0][1][0] for x in obj]

    nums = []
    for bobj in obj:
        # print(bobj)
        nums.append(list(bobj))

    return(np.array(nums))

# data = get_numbers_list(filename)
# printkw() << data[0]
# data.shape

def get_labels_list(tfrecords_filename):
    import numpy as np

    obj = readtf(tfrecords_filename=tfrecords_filename)   
    obj = list(obj)[0].ListFields()[0][1]   
    obj = list(obj.ListFields()[0][1].items())
    obj = dict(obj)['labels']
    obj = np.array(obj.ListFields()[0][1].ListFields()[0][1])

    return(obj)

# labels = get_labels_list(filename)
# printkw() << labels
# labels.shape

# get_ipython().run_line_magic('ll', '.')

