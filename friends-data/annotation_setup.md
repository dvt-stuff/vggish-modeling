--- VLC ---

    GOTO:   vlc_navigation::
                tools > preferences > interface > [show_all] >
                    main_interfaces > [tick rc] > RC > [tick fake TTY] >
                    [UNIX socket input := </path/to/existing/dir/filename>]
                    > [SAVE]

            to pass rc commands and access their output:
                $ echo "[COMMAND]" | nc -U </path/to/socket/from/earlier.sock>

--- jupyter/iPython3 ---

    bash/commandline `magic' [notebook code]:

        > # gets video playing time at function call
        > t = !echo "get_time" | nc -U </path/to/socket/from/earlier.sock>

    ... annotation stuff using the time t
